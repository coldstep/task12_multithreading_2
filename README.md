# Task #

1. Modify exercise 6 from previous presentation to use explicit Lock objects.
2. Modify exercise 7 from previous presentation to use a BlockingQueue instead of a pipe.
3. Create your own ReadWriteLock (or at least simple Lock).
package com.task12_multithreading_2.modified_exercise6;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreeSynchronizedMethod {

  private static long sum = 0;
  private static Lock lock = new ReentrantLock();
  private ExecutorService executor;

  public ThreeSynchronizedMethod(int numberOfThreads) {
    createThreads(numberOfThreads);
    while (!executor.isTerminated()) {
    }
    System.out.println("All threads are finished ,sum - " + sum);
  }

  private void firstMethod() {
    lock.lock();
    System.out.println("Start firstMethod");
    System.out.println("  " + Thread.currentThread().getName());
    for (int i = 0; i < 10000; i++) {
      sum++;
    }
    System.out.println("  sum - " + sum);
    System.out.println("Finish firstMethod ");
    lock.unlock();
  }

  private void secondMethod() {
    lock.lock();
    System.out.println("Start secondMethod");
    System.out.println("  " + Thread.currentThread().getName());
    for (int i = 0; i < 10000; i++) {
      sum++;
    }
    System.out.println("  sum - " + sum);
    System.out.println("Finish secondMethod ");
    lock.unlock();
  }

  private void thirdMethod() {
    lock.lock();
    System.out.println("Start thirdMethod");
    System.out.println("  " + Thread.currentThread().getName());
    for (int i = 0; i < 10000; i++) {
      sum++;
    }
    System.out.println("  sum - " + sum);
    System.out.println("Finish thirdMethod ");
    lock.unlock();
  }

  private void createThreads(int numberOfThreads) {
    Runnable runnable = () -> {
      firstMethod();
      secondMethod();
      thirdMethod();

    };
    executor = Executors.newFixedThreadPool(numberOfThreads);
    for (int i = 0; i < numberOfThreads; i++) {
      executor.execute(runnable);
    }
    finish();
  }

  private void finish() {
    executor.shutdown();
  }

  public static void main(String[] args) {
    System.out.print("Please write number of threads - ");
    new ThreeSynchronizedMethod(new Scanner(System.in).nextInt());

  }
}


